"use strict"


console.log("--------------------------------------------------------");
console.log("Soal 1");
const golden = () => console.log('this is golden!!')
golden()


console.log("--------------------------------------------------------");
console.log("Soal 2");
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()
console.log("--------------------------------------------------------");
console.log("Soal 3");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado", spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


// 4. Array Spreading -----------------------------------------------
console.log("--------------------------------------------------------");
console.log("Soal 4");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

//Driver Code
console.log(combined)


// 5. Template Literals ----------------------------------------------
console.log("--------------------------------------------------------");
console.log("Soal 5");
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before) 
