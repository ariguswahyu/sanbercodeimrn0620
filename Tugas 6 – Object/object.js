"use strict"

console.log("--------------------------------------------------------");
console.log("Soal 1");

var umur = function (tahun) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var result = ''
    if (tahun === undefined || tahun > thisYear) {
        result = 'Invalid birth year'
    } else {
        result = thisYear - tahun
    }
    return result
}

function arrayToObject(orang) {
    var obj = {}
    var key = ""
    for (var i = 0; i < orang.length; i++) {
        key = orang[i][0].concat(" ", orang[i][1])
        obj[key] = {
            firstName: orang[i][0],
            lastName: orang[i][1],
            gender: orang[i][2],
            age: umur(orang[i][3])
        }
    }
    return obj
}

var orang = [
    ["Abduh", "Muhamad", "male", 1992],
    ["Ahmad", "Taufik", "male", 1985],
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"],
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]

console.log("Soal No. 1 (Array to Object)")
console.log("--------------------------------------------------")
console.log(arrayToObject(orang))
console.log("\n")




console.log("--------------------------------------------------------");
console.log("Soal 2");

function shoppingTime(memberId, money) {
    let res = ""
    var obj = {}
    var hargaMin = 50000
    var dataBeli = [
        ['1820RzKrnWn08',
            2475000,
            ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone'],
            0],
        ['82Ku8Ma742',
            170000,
            ['Casing Handphone'],
            120000]
    ]
    if (memberId === undefined || memberId === '') {
        res = "Mohon maaf, toko X hanya berlaku untuk member saja"

    } else if (money < hargaMin) {
        res = "Mohon maaf, uang tidak cukup"

    } else {

        for (var i = 0; i < dataBeli.length; i++) {
            if (memberId == dataBeli[i][0]) {
                obj = {
                    memberId: dataBeli[i][0],
                    money: dataBeli[i][1],
                    listPurchased: dataBeli[i][2],
                    changeMoney: dataBeli[i][3]
                }
            }
        }
        res = obj

    }
    return res
}

console.log("Soal No. 2 (Shopping Time)")
console.log("--------------------------------------------------")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("\n")


console.log("--------------------------------------------------------");
console.log("Soal 3");

function naikAngkot(arrPenumpang) {
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let tarif = 2000
    var arr = []
    var obj = {}

    const jarak = function (awal, akhir) {
        var a = rute.indexOf(awal)
        var b = rute.indexOf(akhir)
        return b - a
    }

    //your code here
    for (var i = 0; i < arrPenumpang.length; i++) {

        let awal = arrPenumpang[i][1]; let tujuan = arrPenumpang[i][2]
        obj = {
            penumpang: arrPenumpang[i][0],
            naikDari: awal,
            tujuan: tujuan,
            bayar: jarak(awal, tujuan) * tarif
        }
        arr.push(obj)
    }
    return arr
}


console.log("Soal No. 3 (Naik Angkot)")
console.log("--------------------------------------------------")
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([]));


