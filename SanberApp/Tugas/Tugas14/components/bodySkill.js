import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import data from "../skillData.json";
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'



function Item({ skillName, category, categoryName, percentageProgress, iconName, iconType }) {
    return (
        <View style={styles.boxLarge}>
            <View style={{ flexDirection: 'row', justifyContent: 'Space-around' }}>

                <Icon name={iconName} size={90} style={{ color: '#003366', marginLeft: 11.5 }} />

                <View style={styles.logoUser}>
                    <Text style={{ fontSize: 24 }} >{skillName}</Text>
                    <Text style={{ fontSize: 16, color: '#003366' }} >{categoryName}</Text>
                    <Text style={{ fontSize: 48, fontWeight: 'bold', color: '#FFFFFF', alignSelf: 'flex-end' }} >{percentageProgress}</Text>
                </View>

                <View style={styles.logoUser}>
                    <Icon name="chevron-right" size={100} style={{ color: '#003366' }} />
                </View>
            </View>
        </View>
    );
}



export default class BodySkill extends Component {
    render() {
        let skill = this.props.data;

        return (
            <View>
                <FlatList
                    data={data.items}
                    renderItem={({ item }) =>
                        <Item skillName={item.skillName} category={item.category} categoryName={item.categoryName} percentageProgress={item.percentageProgress} iconName={item.iconName} iconType={item.iconType} />}
                    keyExtractor={item => item.id}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    boxLarge: {
        justifyContent: 'center',
        width: '100%',
        height: 129,
        marginBottom: 10,
        marginRight: 10,
        backgroundColor: '#B4E9FF',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    },
    text: {
        fontSize: 16,
        color: '#606070',
        padding: 10,
    },
})  