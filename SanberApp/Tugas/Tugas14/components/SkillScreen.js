import React from 'react';
import { View, Text, Image, ScrollView, TextInput, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import BodySkill from './bodySkill';
import Logo from '../../Tugas13/assets/logo.png';

export default class SkillScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.logoWrapper}>
                        <Image source={Logo} style={styles.logo} />
                    </View>

                    <View style={styles.logoUser}>

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: "14px", }}>
                            <Icon name="account-circle" size={40} style={{ color: '#3EC6FF' }} />
                            <View style={styles.logoUser}>
                                <Text style={{ fontSize: 12, fontFamily: 'Roboto', padding: 1, marginLeft: "5px", }} >Hi,</Text>
                                <Text style={{ fontSize: 16, fontFamily: 'Roboto', lineheight: '14px', padding: 1, color: '#003366', marginLeft: "5px", }} >Arigus Wahyu</Text>
                            </View>
                        </View>

                    </View>

                    <View>
                        <Text style={{ fontSize: 40, marginLeft: "14px", padding: 3, color: '#003366' }} >SKILL</Text>
                    </View>

                    <View
                        style={{
                            height: 4,
                            marginBottom: 10,
                            borderBottomColor: "#3EC6FF",
                            borderBottomWidth: 3,
                            marginLeft: "14px",
                            marginRight: "14px",

                        }}
                    />

                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 14, marginRight: 14 }}>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{ fontSize: 12 }}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{ fontSize: 12 }}>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                    >
                        <Text style={{ fontSize: 12 }}>Teknologi</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.body}>

                    <ScrollView style={styles.container} >
                        <BodySkill />
                    </ScrollView>
                </View>


            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },

    header: {

    },
    body: {
        flex: 1
    },
    logoUser: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },

    logoWrapper: {
        alignSelf: 'flex-end'
    },

    logo: {
        width: 250,
        height: 70
    },
    button: {
        alignItems: "center",
        backgroundColor: "#B4E9FF",
        borderRadius: 4,
        paddingHorizontal: 7,
        paddingVertical: 10
    },
    separator: {
        height: 1,
        backgroundColor: '#707080',
        width: '100%',
    },





})