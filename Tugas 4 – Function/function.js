"use strict"

console.log("--------------------------------------------------------");
console.log("Soal 1");
function teriak() {
    return "Halo Sanbers !";
}
console.log(teriak());


console.log("--------------------------------------------------------");
console.log("Soal 2");
function kalikan(angka1 = 5, angka2 = 6) {
    return angka1 * angka2
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2)
console.log(hasilKali);



console.log("--------------------------------------------------------");
console.log("Soal 3");
var introduce = function (nama, umur, alamat, hobi) {
    return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!"
}

var name = "Arigus Wahyu";
var age = 25;
var address = "Purwokerto";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);



