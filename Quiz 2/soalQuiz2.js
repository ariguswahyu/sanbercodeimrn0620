"use strict"

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(subject, points, email) {
        this.subject = subject
        this.points = points
        this.email = email
    }
    average = () => {
        if (Array.isArray(this.points)) {
            let reducer = (accumulator, item) => {
                return (accumulator + item);
            };
            return this.points.reduce(reducer, 0) / this.points.length;
        } else {
            return this.points
        }
    }

}

const obj1 = new Score("Arigus", 7, "arigus@gmail.com");
const obj2 = new Score("wahyu", [8, 9, 6, 10], "wahyu@gmail.com");

console.log(obj1.average());
console.log(obj2.average());


/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/


const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
    let arr = []
    if (subject == 'quiz-1') {
        for (let i = 1; i < data.length; i++) {
            arr.push({ email: data[i][0], subject: subject, nilai: data[i][1] })
        }
    } else if (subject == 'quiz-2') {
        for (let i = 1; i < data.length; i++) {
            arr.push({ email: data[i][0], subject: subject, nilai: data[i][2] })
        }
    } else {
        for (let i = 1; i < data.length; i++) {
            arr.push({ email: data[i][0], subject: subject, nilai: data[i][3] })
        }
    }
    return arr;
}

// TEST CASE
console.log(viewScores(data, "quiz-1"));
console.log(viewScores(data, "quiz-2"));
console.log(viewScores(data, "quiz-3"));


/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student.
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan.
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/


let recapScores = (data) => {
    for (var i = 1; i < data.length; i++) {
        let nama = data[i][0];
        let nilai = (data[i][1] + data[i][2] + data[i][3]) / 3;
        if (nilai > 90) {
            var predikat = "honour";
        } else if (nilai > 80) {
            var predikat = "graduate";
        } else if (nilai > 70) {
            var predikat = "participant";
        }
        let string = `${i}. Email: ${nama}
      Rata-rata: ${nilai}
      Predikat: ${predikat}`;
        console.log(string);
    }
};


recapScores(data);