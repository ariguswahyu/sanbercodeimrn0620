"use strict"

console.log("--------------------------------------------------------");
console.log("Soal 1 : if else");


var nama = "Arigus";
var peran = "Penyihir";



var spasi = " ";
if (nama.length == 0) {
    console.log("Nama Harus diisi");
} else {
    if (peran.length == 0) {
        console.log("Hallo" + spasi + nama + spasi + "Pilih Peranmu Untuk Memulai Game");
    } else {
        let first = "Selamat datang di dunia Werewolf, " + spasi + nama;
        let two = "Hallo" + spasi + peran + spasi + nama + ", ";
        if (peran == "Penyihir") {
            console.log(first);
            console.log(two + "kamu dapat melihat siapa yang menjadi werewolf");
        } else if (peran == "Guard") {
            console.log(first);
            console.log(two + "kamu akan melindungi temanmu dari serangan werewolf");
        } else if (peran == "Werewolf") {
            console.log(first);
            console.log(two + "Kamu akan memakan mangsa setiap malam!");
        } else {
            console.log(first);
            console.log(two + "Kamu memilih peran yang tidak sesuai");
        }
    }
}






console.log("--------------------------------------------------------");
console.log("Soal 2 : switch case");

var hari = 4;
var bulan = 8;
var tahun = 1995;

if (hari >= 1 && hari <= 31) {
    if (tahun >= 1900 && tahun <= 2200) {

        switch (bulan) {
            case 1:
                bulan = "Januari";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 1:
                bulan = "Februari";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 3:
                bulan = "Maret";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 4:
                bulan = "April";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 5:
                bulan = "Mei";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 6:
                bulan = "Juni"
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 7:
                bulan = "Juli"
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 8:
                bulan = "Agustus";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 9:
                bulan = "September";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 10:
                bulan = "Oktober";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 11:
                bulan = "November";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            case 12:
                bulan = "Desember";
                console.log(String(hari).concat(spasi, bulan, spasi, String(tahun)));
                break;
            default:
                console.log("bulan : angka antara 1 - 12");
                break;
        }

    } else {

        console.log("tahun :  angka antara 1900 - 2200");
    }

} else {
    console.log("tanggal :  angka antara 1 - 31");
}




