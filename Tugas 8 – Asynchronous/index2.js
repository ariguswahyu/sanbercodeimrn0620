"use strict"


var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0
var waktu = 10000
var tagihJanji = function (a, b) {
    if (i < b.length || a > 0) {
        readBooksPromise(a, b[i])
            .then(function () {
                tagihJanji(a, b)
            })
            .catch(function (err) {
                console.log('time is up')
            })
    }
    a -= b[i].timeSpent
    i++
}

tagihJanji(waktu, books)
