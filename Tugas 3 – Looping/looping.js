"use strict"


// SOAL 1
console.log("--------------------------------------------------------");
console.log("Soal 1 Looping While");


var start = 2
console.log('LOOPING PERTAMA')
while (start < 21) {
    console.log(start + ' - I love coding')
    start += 2
}

var start = 20
console.log('LOOPING KEDUA')
while (start > 1) {
    console.log(start + ' - I will become a mobile developer')
    start -= 2
}




// SOAL 2
console.log("--------------------------------------------------------");
console.log("Soal 2  Looping menggunakan for")
console.log('OUTPUT')
let word = "";
for (var i = 1; i < 21; i++) {

    if (Math.abs(i % 2) == 1) {
        if (i % 3 == 0) {
            word = 'I Love coding'

        } else {
            word = 'Santai';

        }

    } else if (i % 2 == 0) {
        word = 'Berkualitas'

    }

    console.log(i + ' - ' + word)
}



// SOAL 3
console.log("--------------------------------------------------------");
console.log("Soal 3 Membuat Persegi Panjang")

for (var i = 0; i < 4; i++) {
    console.log('#'.repeat(8))
}



// SOAL 4
console.log("--------------------------------------------------------");
console.log("Soal 4 Membuat Tangga")
for (var i = 0; i < 8; i++) {
    console.log('#'.repeat(i))
}

// SOAL 5
console.log("--------------------------------------------------------");
console.log("Soal 5 Membuat Papan Catur")

for (var i = 0; i < 8; i++) {
    if (i % 2 == 0) {
        console.log(' #'.repeat(4))
    } else {
        console.log('# '.repeat(4))
    }
}

